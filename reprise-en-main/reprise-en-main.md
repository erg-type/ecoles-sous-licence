# Reprise en main

In this Post Script research session, we looked specifically at the practice of revival, asking the question: what is the intention of the typographer? 
John Downer's essay, ***[Call It What It Is](https://www.emigre.com/Essays/Type/CallItWhatItIs)***, presents the different nuances of revivals. To capture all these nuances, we illustrated these different categories by looking for specific examples.

This text was first published in 2003 in the type specimen booklet for Tribute.


A discussion of typeface sources seems to pop up whether a designer admits to being inspired by historical models or not. Getting the appropriate authorization when needed, and giving the proper credit, are but two of many considerations. Other issues such as fidelity to the model, chronological accuracy, and the pros and cons of revisionist history get debated and argued at length. The talk can get hot. Designers always feel the heat.

On the one hand, a type designer who makes a serious effort to acknowledge certain sources of inspiration opens himself or herself to criticism concerning the ethics of appropriating the work of another. On the other hand, a type designer who fails to cite sources, or, worse, makes a conscious effort to avoid acknowledging sources, leaves himself or herself open to charges of impropriety.

One may ask, “Is there no safe and sound route these days?” I believe there is. In fact, I think there are several good roads.

To understand the intrinsic differences between plagiarism (normally regarded as a bad thing) and preservation (normally regarded as a good thing), we should look at various means by which newer typefaces are derived from older ones. There are indeed many approaches. Outlining them can be helpful in considering the practices surrounding revivalism in general.

The integrity of a typeface revival depends not solely on what the designer does to create a workable version of an old idea; it also depends on what the designer, or the designer’s copywriter or publicist, has to say about the genesis of the design.

If ad copy, or whatever prose is written to launch a typeface, is inaccurate or misleading, there might be grounds for a dispute. In contrast, if the story behind the designer’s effort stands up to the scrutiny of type historians and scholars, a revival has a far better chance of being considered a welcome addition to the world of revivals - not so much for being a “servant” to a given typographic model as for bearing a relationship to its history.

Historians regard type history in ways that type designers and type critics seldom do. This theme was articulated in a keynote address at the 2002 conference of the Association Typographique Internationale (ATypI) in Rome by Paul F. Gehl, historian and curator of a type-history collection at The Newberry Library, in Chicago.

In his talk, Gehl noted that type experts (including some effective and influential type promoters, I should add), have been known to give imprecise descriptions and fabricate misnomers. Monotype’s introduction in 1929 of a typeface series known as “Bembo,” based on the first roman type of Aldus Manutius, circa 1495, was cited by Gehl as an opportunity for Stanley Morison, the typographical advisor to Monotype, to inaccurately characterize Bembo, as he did with other historically-based typefaces by Monotype in the 1920s. Morison, according to Gehl, “... insisted upon calling his historical reconstructions of the 1920s ‘recuttings’ of early types, when in fact most of them were beautiful new types inspired by handsome old ones.”

This observation strikes a familiar chord among type reviewers. Accuracy often hinges on semantics, so semantics are important.

It seems that the term “recutting” could be accurately applied to a faithful recreation, if it were cut by hand and cast in metal, but that is not exactly what has been done in the process of creating usable facsimiles of centuries-old type. To do a “recutting” in the most literal sense of the word would ostensibly require a cutter of type to work in the same manner, and with the same materials, as the originator. The term “recutting” has come into modern usage partly by way of inheritance and partly by way of convenience. There is no real cutting being done by makers of digital typefaces; namely, faces meant to be fully accepted as recreations of former glories.

In the digital medium, a medium without the physicality of sculpture, what’s attainable can be but a silhouette of facial features produced by carving type at the size - the only size - it will print, in relief, in reverse, in steel. Unlike cutting away excess material to render the form desired, digital type is shaped by manipulating on-screen descriptions of contours. Any “digital recutting” takes place merely in a figurative sense.

But don’t let pure semantics completely limit our abilities to label today’s digital replicas of historical types in real and fitting ways. Apt descriptions are almost always possible if there exists a broad vocabulary from which to establish appropriate terminology. We still need new nomenclature for the digital era to replace outdated language that has lost its meaning or has taken on an erroneous twist. Oxymorons like “digital punchcutter” and “digital type foundry” are common in the trade, but at least they have the word “digital” as a qualifier. That’s a lot better than not having a qualifier.

The same may be said of the common term “revival” in describing updates of typefaces that never fell completely into disuse before being converted to a new medium. Labeling a typeface “digital revival” lets us know that the original was born in a pre-digital medium, most often metal. To do a revival in type is to resurrect a design that has fallen into disuse, not to rehash a workable design that never became obsolete or outdated. As Gehl has noted, “Let’s just resolve not to call them historical ‘reproductions,’ ‘recuttings,’ or even ‘re-designs’ unless we intend to do just that, reproduce a type that works like the original.”

Gehl further remarked, “... In my professional capacity as collector, I frequently meet with designers and design teachers and students. What I have to say today is thus conditioned not by my sense of what you as typographers and type writers are doing right or doing wrong, but by my reading of what practicing designers and design students make of what you do and say about type.”

On that cue, a few definitions would be handy. Below are mine. I’ve divided my descriptions into two categories: one for designs that closely follow the original, and the other for designs that loosely follow the original.


## 1.

## Revivals/Recuttings/Reclamations

Closely based on **historical models** (metal type, hand-cut punches, etc.) for commercial or noncommercial purposes, with the right amount of **historic preservation and sensitivity to the virtues of the original** being kept in focus - all with a solid grounding in type scholarship behind the effort, too.

> ### Chaumont Script
> * Alexandre Bassi 
> * 2021
> * [ANRT](https://anrt-nancy.fr/anrt-22/fr/fonts)
> * SIL Open Font License (OFL) 
> * Timothée Gouraud (Fabrication Maison) discovered in 2016 in Chaumont the panels and signs made by Chantal Jacquet, a letter painter, in the 1980s. Intrigued by these singular forms, he contacted the ANRT to make a digital font of them. A considerable work, realized by Alexandre Bassi, who managed to translate the cursiveness and the rhythm of this writing, thanks to an extended glyph set (1500 glyphs) and advanced OpenType functions.

## Anthologies/Surveys/Remixes

Closely based on characters from **various fonts** all cut by one person, or cut by various hands, all working in one particular style or genre - **like a medley or an overview done more for the sake of providing a “sampling”** than for the sake of totally replicating any one single cut of type.

> ### Compagnon
> * Juliette Duhé, Chloé Lozano, Valentin Papon, Léa Pradine, Sébastien Riollier
> * 2018
> * [Fonderie U+270D](https://u270d.eesab.fr/caracteres/)
> * SIL Open Font License (OFL) 
> * Compagnon is inspired by the online specimen archive Typewriter Database and brings together different moments in the history of typewriters. Each weight is based on singular references related to significant periods, showing the evolution of the typewriter.

## Knockoffs/Clones/Counterfeits

Closely based on commercial successes (of any medium) to belatedly muscle in on part of an unsaturated market, often by being little more than a cheap imitation of what has already been deemed by experts as a legitimate revival. “Me Too” fonts, or “Copy Cat” fonts, as they are called, **tend to focus on opportunism rather than on originality**. These don’t rate as revivals because they don’t revive.


## 2.

## Reconsiderations/Reevaluations/Reinterpretations

Loosely based on **artistic successes** (of any medium) **as a kind of laboratory exercise**, often without much concern for their immediate or eventual commercial viability.

> ### Aldiviva
> * Victor Gérard
> * 2020
> * [Atelier de Typographie - La Cambre](https://lacambretypo.be/fr/typefaces)
> * Unknown
> * Aldiviva is a modern interpretation of Vivaldi typeface, originally designed by Peter Friedrich in 1994. Aldiviva consists in 4 weights: Primavera, Estate, Autunno, Inverno.

## Homages/Tributes/Paeans

Loosely **based on historical styles and/or specific models**, usually with admiration and respect for the obvious merits of the antecedents - but **with more artistic freedom to deviate from the originals** and to add personal touches; taking liberties normally not taken with straight revivals.

> ### Robbery
> * d-e-a-l
> * 2023
> * [Fonderie U+270D](http://adve.tf/frontpage/#robbery-preview)
> * Unknown
> * Robbery is a typeface based on a drawing of Henry Van de Velde for the book "Ecce homo" by Friedrich Nietzsche.

## Encores/Sequels/Reprises

Loosely **based on commercial successes** (of any medium) as a means of **further exploring, or further exploiting, an established genre**; milking the Cash Cow one more time.

> ### Neue Helvetica
> * Monotype
> * 2017
> * [Monotype](https://www.linotype.com/fr/1266/neue-helvetica-famille.html?gclid=EAIaIQobChMI-_WR2I_K_QIVhOd3Ch2t3A6vEAAYASAAEgKalfD_BwE)
> * All Rights Reserved (EULA)
> * Neue Helvetica is a melding of aesthetic and technical refinements that result in superior design proportions, improved legibility and an expanded range of uses beyond the original Helvetica typefaces

## Extensions/Spinoffs/Variations

Loosely **based on artistic or commercial successes** (of any medium) for only rarely **more than minor advancements in a tried, popular, accepted style**; akin to previous category.

> ### Baskervville
> * Alexis Faudot, Rémi Forte, Morgane Pierson, Rafael Ribas, Tanguy Vanlaeys, Rosalie Wagner
> * 2017–2018
> * [ANRT](https://anrt-nancy.fr/anrt-22/fr/fonts)
> * SIL Open Font License (OFL) 
> * Baskervville is a revival of Claude Jacob's typefaces. First distributed by the Rolland & Jacob foundry in Strasbourg at the end of the 18th century, Jacob's typefaces were then distributed by the Société Typographique Levrault (then Berger-Levrault) under the name of "Characters in the Baskerwille genre" - with a w. The particularity of Jacob's "Baskerwille" is that its roman is very close to the original Baskerville, while the italic is close to a neo-classical italic of Didot. Redrawn as part of a research program and workshop at ANRT, Baskervville is a revival of revival, a transitional typeface.

>> ### Baskervvol
>> * Bye Bye Binary
>> * 2021
>> * [La typothèque Bye Bye Binary](https://typotheque.genderfluid.space/baskervvol.html)
>> * OIFL (Open Inclusif·ve Fonte License)
>> * Baskervvol is a reworking by BBB of the National Typographic Research Workshop (NTRA) Baskervville, itself a reworking of Claude Jacob's 1784 Baskerville, designed by John Baskerville in 1750. John Baskerville is an exemplary case of the invisibilization of women in the history of typography. Sarah Eaves, his companion and business partner, who took over the printing business after Baskerville's death, has never been credited for her work even though she was heavily involved in the typefaces and prints marketed by her husband (1). In 1996, typographer Zuzana Licko would design a typeface in her honor. Since 2018, Baskervvol has been collectively augmented with inclusive glyphs. Where universities require the use of Times New Roman, privately licensed and rights reserved, for writing scientific papers, the use of Baskervvol, a typeface with similar stylistic and historical authority, but liberated by its license, allows for the introduction of non-binary glyphs in normative places of knowledge dissemination.

## Caricatures/Parodies/Burlesques

Loosely **based on prominent features of the model**, often **with humor or satire as the primary objective**, but quite often also with humor or satire as an unexpected effect.

> ### Stanley Smith
> * David Bennewith
> * 2016
> * [Site de David Bennewith](https://colophon.info/)
> * SIL Open Font License (OFL)
> * ‘Stanley Smith’ represents a hard-graft of Stanley Morison’s ubiquitous typeface ‘Times New Roman’, and Adidas’ ubiquitous tennis shoe ‘Stan Smith’. Departing from an observation, with the desire to produce a visual critique, of the often strange anthropomorphisms applied to alphabetical letterforms, ‘Stanley Smith’ bluntly assumes a type design more connected to histories of mass-production than corporeal gesturing.


Centuries ago, loose interpretations were easier to produce than close (faithful) interpretations because the level of skill needed to produce punches was high. But late in the 19th century, the use of the pantograph as a tool in cutting punches and matrices by machine eliminated the need for a punchcutter who worked by hand. The speed of replicating existing typefaces increased. Phototype was yet another step in the direction of fast copying, and digital type can be copied in an instant by almost anybody.

Our ability to make digital facsimiles of types that were cut by hand centuries ago affords us a chance to render them as we see fit. We can make them look old, like the original types, or we can make them look fresh. We can’t, however, make them look identical to historical models, for digital type is not metal type. The two are different creatures and they manifest separate identities. They each have their own idiosyncrasies.

Realizing that digital type can actually only simulate the “look” of old type is an important aspect of evaluating type revivals. Terms like “digital homage” or “historical fiction” can be used to describe what we attempt to do when we pay tribute to types of the distant past without relying too heavily upon their design.

It is evident that Frank Heine’s Tribute possesses an element of “type caricature” in its drawing, but this fact doesn’t relegate it to that one category. Heine has really gone beyond parody, well into an area of personal exploration. He has challenged many traditional assumptions that we “connoisseurs” of hand-cut type have maintained in our attitude toward the historical accuracy sought and loved and expected in “revivals.” The result is a unique combination of caricature, homage, alchemy, and fanciful reinterpretation.

Tribute, I think, recalls Guyot’s native French-learned style, primarily as a point of departure for an original - albeit implausible - work of historical fiction, with merits and faults of its own.
