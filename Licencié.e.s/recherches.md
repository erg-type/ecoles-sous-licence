# Rendre lisibles, documenter l'accès et la compréhension des licences

## Premier constat : Manière d'expliquer les licences sur les sites de font

### -Par mail : Questions about licence, please mail me  https://ohnotype.co/info/licenses/desktop
### -FAQ : pangramprangram : https://pangrampangram.com/pages/faq#font-licensing
### -Au premier plan : Bye-Bye binary : https://typotheque.genderfluid.space/
### -Avec un lien vers le texte de licence : ANRT : https://anrt-nancy.fr/anrt-22/fr/fonts

## Différentes licences

### OFL compatible que avec OFL -> si tu modifies une fonte OFL, tu dois la diffuser sous OFL
### Licences privatives EULA -> "End user" 
### Licences Art Libre 
### CC4r -> réecriture de la licence Art Libre
### Genderfail
### ACAB licence
### Licence Amicale

## Comment sensibiliser les étudianx ?
##### Proposer une licence étudiante ?
##### Apéro licenses en écoles d'art
##### Trouver des "mises en formes" de licences FUN

## comment y a-t-on été sensibilisé ?
### Via des conseils de profs?
### Via des recherches sur le sites des fonderies?
### Via des ressources exemple : git de l'erg?
### Le site de fonte de l'école utilise déjà la OFL par exemple donc j'utilise l'OFL?
### Via ça http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL

## Des licences lisibles!
### Je suis étudianx
### Je veux publier une fonte
### Quelle licence choisir?
### Qu'est-ce que je veux que ma fonte "devienne"
### Pour qui?

### Créer un guide de la licence pour étudiant.es 
### qui serait téléchargeable en pdf - mis à disposition dans chaque école
### Explication, vulgarisation
### Une fonte = un logiciel
### Dessin attribué à un.e auteur.ice protégé par droit d'auteur, fichier fonte -> logiciel protégé par license
### Droit d'auteur toujours au dessus de la licence
### qu'est-ce qu'une licence, à quoi ça sert
### Qu'est-ce qu'elle peut contenir
### Exemple : Guide d'auto-défense pour étudiant.es en école d'art - Les mots de trop




## Créer un site qui répertorie plusieurs licences différentes avec des mots clés/dessins/glyphes/logotypes associés à chaque licences qui puissent nous permettre de trouver laquelle nous convient, lesquels sont plus ou moins semblables?
### mots clés/dessins/glyphes/logotypes -> peuvent aussi être "humoristiques" en fonction des ambitions de la diffusion de la fonte, exemple : 
### viser une utilisation par toustes : libres
### viser une utilisation projets "politiques" : 
### utilisation pour projets qui ne font pas de profit dits "capitalistes"
### utilisation que par d'autres étudianx

#### commercial use ? 
#### use-modify ?
#### Re publier ?
#### Cadre enseignement


## Je suis étudianx -> je veux utiliser une fonte -> Quelle fonte je choisi en fonction de quelle licence?
### Même site que dit précédemment mais fontes sont associées à des mots clés/dessins/glyphes/logotypes en fonction de leurs licences -> permet de choisir une fonte en fonction de sa licence, en fonction de ce qu'on veut "faire" avec la fonte
### Partie choix de licence, avec plusieurs choix (comercial use, modify, combien exemplaire, republication)
### Partie recherche de font, en fonction de cadre d'études, fanzine peu d'ex, publication web local. 
### Met le choix de la licence avant le choix de formes de fontes
### Point d'entrée avec un systèmes de valeur, d'éthique etc.
### et les mêler avec des choix binaires

# do what the fuck you want




