# JE SUIS ETUDIANX
## JE VEUX PUBLIER UNE FONTE
### QUELLE LICENCE CHOISIR?
### POUR MA FONTE : quels seronts les usagers, usages, contextes, modifications (ou non), diffusion/échelle
#### ETHIQUE
##### USAGERS
###### Toustes sans distinction
###### Communautés spécifiques
__Gender fail protest fonts__
###### Toustes sauf ... (exemple : Pinkest pink)
###### ...

##### USAGE
###### non-commercial
###### commercial
###### ...

##### CONTEXTE
###### ecoles
###### entreprises
###### politique
###### amis
###### culturel
###### associatif
###### ...

#### MODIFICATIONS
##### non ouvrable/modifiable
##### libre de tout usage 
__WTFPLicense__
##### modifiable
##### conversion fichiers
##### créations masters
##### modifier spacing/kerning
##### Modifier forme fonte
__OFL__
##### ...
    
#### DIFFUSION/ECHELLE
##### 100 utilisateurices
##### mondial
##### mon entourage
##### école
##### ...
