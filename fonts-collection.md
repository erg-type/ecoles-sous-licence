# Quelques fonderies et collections 



## Fonderies libres

[bboxtype](https://bboxtype.com/typefaces/?filter=FFF)
bBox Type is the studio for type development of Ralph du Carrois in Potsdam.

[Berzulis](https://berzulis.com/) is an ongoing experimental type foundry project with a focus on Lithuanian mythology and alphabet.

[Bonjour Monde](https://gitlab.com/bonjour-monde/fonderie)

[Cat fonts](http://www.peter-wiegel.de/)  

[Collletttivo](http://collletttivo.it/) 

Collletttivo est un groupe de designers travaillant sur des projets typographiques et les publiant occasionnellement sous licences Open Source. Les membres fondateur·ices sont basé·es en Italie.

[Dotcolon](http://dotcolon.net) 

[Fonderie downlaod](https://www.fonderie.download/)  
[Fonderiz](https://fonderiz.fr/)  
[Hanken](https://hanken.co)  
[Impallari Type](https://github.com/impallari?tab=repositories)  


[Lobbby24](https://lobbby24.com/Startseite) Lobbby24 is Simon Marcio Bretz,  Hannes Brischke and Florian Budke - Mannheim

[Not your type](https://notyourtype.nl/typefaces/) 

[Monti Fonti](https://montifonti.tilda.ws/)

[OSP foundry](http://osp.kitchen/foundry/)  
[Phantom foundry](http://phantom-foundry.com/) 

[quarantine-fonts](https://github.com/jenskutilek/quarantine-fonts) FJens Kutiliek type designers' drawer archived during the pandemic quarantine in 2020.

[RORY KING](https://rorykingetc.com/dasickfonts)
[Tunera](http://www.tunera.xyz/)  
[Warsaw type](https://kroje.org/en/)  


## Collections libres

[Badass Libre Fonts by Womxn](https://www.design-research.be/by-womxn)  
Cette collection vise à donner de la visibilité aux fontes libres dessinées par des designers féminins, qui sont souvent sous-représentées dans le domaine traditionnellement conservateur de la typographie. 

[Bye Bye Binary](https://typotheque.genderfluid.space/)
[Luuse](http://typotheque.luuse.io/)  
[Velvetyne](https://www.velvetyne.fr)   
[Fontain par Lakkon](https://fontain.org/) 
[Font Library](https://fontlibrary.org)  
[Font Squirrel](https://www.fontsquirrel.com)  
[Goofle fonts](https://fonts.google.com)  
[Open Foundry](https://open-foundry.com)  
[La typothèque de TeX](https://www.tug.org/FontCatalogue/allfonts.html)  
[The league of moveable type](https://www.theleagueofmoveabletype.com/)  

[Republish](https://republi.sh)
Free and open source fonts inspired by vietnamese vernacular typography, designed by various authors.

[Tunera](http://www.tunera.xyz/) 
[Uncut](https://uncut.wtf/)  

[Use and modify de Raphaël Bastide](https://usemodify.com) 
"Ceci est une sélection personnelle de caractères beaux, classes, punks, professionnels, incomplets, bizarres. Les licences open source les rendent libres d'utilisation et de modification. Cette sélection est le résultat de recherches profondes et de coups de cœur. Cette sélection est la vôtre." 


### Monospaces

[Codeface](https://github.com/chrissimpkins/codeface)

[The anatomy of a Thousand Typefaces](https://useratio.com/the-anatomy-of-a-thousand-typefaces/app/)

[Victor mono](https://rubjo.github.io/victor-mono/)


### Single-lines 

[Single line](https://github.com/isdat-type/Relief-SingleLine) isdat Toulouse 

[Routed Gothic](https://webonastick.com/fonts/routed-gothic/)


### Neuro-atypiques

[DYSLEXIA SCOTLAND](https://www.nothingcomicaboutdyslexia.com/inconstant-regular)


## Collections choisies

Flinta*
[flint*ype](https://www.flintype.com/) is an ongoing project by Lilot Kammermeier, Sophia Krayc, Coco Lobinger and Hannah Witte, with the technical support of Laila Kamil. Its pre-launch was set at the Academy of Fine Arts Leipzig (HGB).

## Schools!

[ANRT](https://anrt-nancy.fr/en/fonts) 

[Chasse ouverte](http://chasse-ouverte.erg.be), la fonderie de l'ERG 
dont version précédente reste en ligne [ici](http://copyright.rip/erg/typo/)

[E162](http://e162.eu/fonderie), fonderie d'Estienne 

[ECAL](https://ecal-typefaces.ch/) 

[ESAD Amiens](http://postdiplome.esad-amiens.fr/) 

[KABK Master type and media](https://www.kabk.nl/en/programmes/master/type-and-media)  https://www.typemedia2018.com/

[La Cambre](http://lacambretypo.be/fr) 

[No foundry](http://nofoundry.xyz)   
Visual Communication department at HfG Karlsruhe “NoFoundry is a website for student type designers, enabling them to exchange ideas and sell fonts on their own terms. ” 

[2000 upm](https://2000upm.uk/), une initiative d'étudian* pour financer leur jury de diplôme (Kingston School of Art, Kingston Upon Thames, UK)

[SUVA](https://www.suvatypefoundry.ee/) à Talinn, Estonie 

[Reading university](http://typefacedesign.net/typefaces/) 

[La typothèque du 75](http://typotheque.le75.be/) 

[U+270D](https://u270d.eesab.fr) à l'EESAB - Rennes

[X Cicéro](https://xcicero.esad-gv.net/index.php) est un atelier de dessin de caractères proposé aux étudiant·es de l’option design graphique de l’ESAD Valence, construit autour de la petite collection de caractères en bois de l’atelier d’impression typographique de l’école. 

## Entre deux

[Future Fonts](https://www.futurefonts.xyz/) 

dont [Love letters](https://www.futurefonts.xyz/loveletters)

[Noir Blanc Rouge](https://noirblancrouge.com) 

[Quelques fontes libres](http://typefaces.temporarystate.net/preview/) chez Roman Gornitsky de The Temporary State , juste à côté des privatives et aussi [les textes](http://letters.temporarystate.net/) 

[Death of Typography](https://deathoftypography.com/typefaces/) A young Singaporean type collective.

## Independent type foundries

[Grillitype](https://www.grillitype.com/) par exemple https://www.gt-maru.com/

[BB-Bureau](https://www.bb-bureau.fr/) 

OurType Belgium-based type foundry established in 2002 by Fred Smeijers.

[Bold Monday](https://boldmonday.com/) 

[The designers foundry](https://thedesignersfoundry.com/fragen)  

[Lineto](https://lineto.com/) 

[Colophon Foundry](https://www.colophon-foundry.org/) (us) 

[P22](https://p22.com/) 

Hoefler & Co. https://www.typography.com/

Klim Type Foundry (New Zealand) https://klim.co.nz/

[Or type](https://ortype.is/) (Bruxelles from danemark/islande) 

[A2-Type](www.a2-type.co.uk) (London) 

[Swiss Typefaces](https://www.swisstypefaces.com) 

[Black foundry](https://black-foundry.com/fonts/) 

L'imprimerie nationale de France https://twitter.com/ANRT_type/status/1186253028434874368)

[Plain form](https://plain-form.com/)
    
[Paratype](https://www.paratype.com/)

    
[Out of the dark](https://www.outofthedark.xyz/)




## Independants type designers

[Luca Bihan](https://fonderiebretagne.fr/)

[A is for fonts](https://aisforfonts.com/fonts) Émile Rigaud 

[Roxane Gataud](https://www.roxanegataud.com/) 

[Studio Cryo](https://berzulis.com/), Lithuanian mythology and alphabet

[Lucas Descroix](https://www.lucasdescroix.fr/) 

[Radim Peško](https://radimpesko.com/) aka RP Digital Type Foundry  

Justin Bihan, [Lettres simples](http://www.lettres-simples.com/) 

[Yann Linguinou](https://yannlinguinou.com/) 

[Lucas le Bihan](https://bretagnebretagne.fr/) 

[Eliott Grunewald](https://eliottgrunewald.xyz/) 

Erik Marinovich, [Nuform](https://nuformtype.com/) 

[Glyphworld](http://www.glyphworld.online/) Leah Maldonado 

[Rory King](https://rorykingetc.com/dasickfonts)






## Meta meta : outils


[V-fonts](https://v-fonts.com/)
Une review de fontes variables.

[Ofont](https://github.com/raphaelbastide/ofont)

"Ofont is a free and open source tool for font collections. It runs in the browser and is easy to install on your server.


## Recherches et séminaires

ENSAD Nancy 
https://vimeo.com/ensa
le Viméo de École Nationale Supérieure d'Art et de Design de Nancy (France)