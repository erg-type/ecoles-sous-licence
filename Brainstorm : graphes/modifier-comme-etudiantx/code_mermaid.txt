%% https://mermaid.live/edit#pako:eNpVk8-O2jAQxl_F8qWuBFucfwu5VEC2VavdCokedqtc3MRJrBo769jtsizv0mN5Dl6sxnEkckH5zQzfzHzJHGAhSwpTWHH5p2iI0uB7lgsAwCN6kCWrGFXACAr0vpW1Im3D6HuXf0J3AmgiNHg2784nbUpm4aVPPoLpdAqecuFoidFWk5qCD2DJNVWCiMKrLAO0oaqTA2GUsaoy3fk0RGwB6UA5joboK71MRQrNqt-DVoS2heSEKR-w_3ZzWFkv5jHoN-gJX8MoE15DZMHvszpk51OtzidQUrBzLhVEMynyXNgIJ84t8PHot3cCq-sRPITXEA3gcI3RXVVR3QHa6cb6y54N7YDtYJsOvTmrNBN1v-06QMvsG7gaYFzcGkX14CBYuW5vD9JwCs5_QU00JeblbY1H-a0pFAU1JwV9W_fmuJ8Mo8983zZdL5cF6JMU-p789ByiL5ybTiuipfKxCG0aqWXXyNaNtiFMaJ-L0bKyXwa47Fzofs-Nojt2_je8zSxB97JmBaMchJmrWHFqDb_oe8_c0BkeUTCi0HvVUzSieESJ38RXwgncUbUjrLTXcrjkcqgbuqM5TO1jSdSvHObiaOuI0XK7FwVMtTJ0Ak1bWnczRuwB7WBaEd7ZKC2Z9eahPz93hRPYEgHTA3yBaYRvFjHGyW00u02iKA4ncA_TIL69iXE4n8_myTzEySw-TuCrlFZ1djMPwjCZ4TBI5vFiEcVO7odLupbH_2vuKDI %%

flowchart TD
   X(Modifier une typographie)
   Y(En tant qu'étudiantx)
   X --- Y

   A1(Stage / Alternance)
   A2(Perso)
   A21(Diffusé)
   A22(Pas diffusé)
   A3(Jeune actifve)
   A4(Scolaire)
    A2 --- A21
   A2 --- A22
   Y --- A1
   Y --- A2
   Y --- A3
   Y --- A4
 

   B{Dégré de modification\nde la typo ?}
   A1 --- B
   A2 --- B
   A3 --- B
   A4 --- B

   C1(Effets esthétiques \n degré de lifting)
   C2(ADN de la typo \n degré de pureté)
    B --- |Moule à gateaux|C1
    B --- |Sucre glace|C2
   
   D1(Glyphs)
   D2(FontLab)
   D3(Illustrator)
   D4(Photoshop \n Paint)
   D5(After Effects \n Première)
   D6(Logiciel 3D \n Blender)

   C1 --- D1
   C1 --- D2
   C1 --- D3
   C2 --- D4
   C2 --- D5
   C2 --- D6
   D3 --- D4
