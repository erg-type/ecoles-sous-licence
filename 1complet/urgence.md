### Intro
Nous avons tenté de lister et proposer des solutions pour résoudre les problèmes de glyphset incomplet. Nous avons trouvé différentes manières de résoudre ce problème selon différents différents niveaux d'urgence. Ceux-ci sont définis selon le temps dont nous disposons dans le cadre d'un projet. Il nous contraint mais moins nous en avons, plus nous nous autorisons une grande liberté. Ces solutions ne sont pas légalement viables et ne respectent pas les normes typographiques. Ce sont des propositions plutôt pour des projets personnels/non rémunérés. Cependant, les solutions qui prennent le parti d'assumer pourraient tout à fait être appliquées de façon professionnelle. 


# Outils pour éviter l'urgence

* vérifier ce dont dispose la font avec FontDrop!
https://fontdrop.info/#/?darkmode=true

* par un système d'émoticône, signale la présence de glyph ou non (€, eszett, …)
http://www.peter-wiegel.de/Fonts/index.html 

* utiliser une typo qui joue avec le texte à sa manière : ex résistance (velvetyne)



## Urgence maximale — libertés permises ++

### (assumer) utiliser un glyphe déjà existant pour remplacer le glyphe manquant**

exemples : 
§ 
* (maj $)
∂ (alt d)
¬ (alt L minuscule)
≈ (alt x)
◊ (alt v)
{} (alt ())
¶ (alt §)
ø (alt à)

**possibilité 1**
il n'y a pas d'accent je remplace le é par ∂ et le ô par ◊
iel a d∂ssin∂e un h◊tel

**possibilité 2**
je remplace tous les glyphes manquants par le signe que tu veux
iel a d≈ssin≈e un h≈tel
->>> cf. urgence minimale : créer une glyphe qui remplace le glyphe manquant


### (assumer) Utiliser une typo existante qui n'a rien à voir**


### (cacher) Utiliser une typo existante ressemblant à celle utilisée** 


## Urgence moyenne — entre les deux

### (cacher) Dans le logiciel de mise en page copier coller un accent/signe manquant bricolé.**
(exemple glyphe Dollar produit en utilisant le S et y rajouter 2 barres) 



## Urgence minimale — quand on a du temps et qu'on peut ajouter des glyphes sur glyphs

### (assumer) dessiner un glyphe indiquant un glyphe manquant**

### (cacher) ouvrir la font dans Glyph pour rajouter des accents/ou glyph manquants**

### (assumer) contacter la fonderie pour leur demander les glyphs spécifiques nous manquant, expliquant notre projet**

### mode d'emploi pour écrire un mail type pour envoyer un mail à une fonderie

### lien vers un tuto qui explique comment faire des accents dans un logiciel de dessin caractère
