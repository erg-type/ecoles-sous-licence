# Groupe Typothèque écoles


### PROPOSITION MANIFESTE DE GUIDE DE PUBLICATION DES TRAVAUX TYPOGRAPHIQUES ETUDIANX NON ABOUTIS

> "j'ai travaillé sur une fonte que je n'ai pas terminée. J'ai donc plusieurs dizaines de glyphes mais qui ne sont pas utilisables en tant que fonte par quelqu'un.e d'autre. Que faire de ce projet que j'ai mis de côté et qui a du potentiel ?"

> "Vous n'arrivez pas à capitaliser sur vos projets de typos non terminées ? Nous avons **LA** solution. On vous propose de recycler vos lettres non usagées en circuit court auprès d'autres étudianx."


### Pourquoi les typos non-finies sont intéressantes et pourquoi méritent-elles d'être publiées ?

La typographie est un outil et s'incarne donc dans l'utilisation et dans la juridiction comme un logiciel. Cependant elle se base également sur des dessins. Ainsi, même si un travail typographique n'abouti pas, il comporte des formes intéressantes qui pourraient intéresser d'autres designeureuses pour d'autres projets.

Site qui affiche le nombre de caractères d'une fonte: https://fontdrop.info/#/?darkmode=true
> "J'ai plein de petits projets pas assez conséquents pour les publier sur mon portfolio. Q'en faire?"

> "Moi j'ai découvert plein de portfolios comportants une section 'expérimentations' à la fin qui répertorie tous ces types de projets."


### Comment ces formes et ces lettres peuvent être (ré)utilisées ?

Utiliser, scanner, changer, modifier, détourner. Ces formes peuvent prendre part à une grande diversité de projet. Si continuer ou terminer la fonte est possible, ces formes représentent surtout une richesse graphique qui peut prendre part à divers projets.

> "Pour la conception de la communication de la journée d'étude Post $cript à la HEAR Strasbourg, le signe $ de la police Coupeur de Paol Darcel (erg) nous intéressait par sa forme particulière, on a donc repris ce signe seul pour le réimplémenter dans une animation Processing"


### Typothèque V.S. registre de lettres/formes

La platforme que nous envisageons n'est pas une typothèque car elle ne répertorie par des typographies mais bien des formes, des formes de lettres en l'occurence. Ce sont d'abord des images et pas des outils comme les fichiers typographies classiques qui sont des logiciels pensés pour être installés et utilisés.

> "Il ne faudra pas reprendre les esthétiques des fonderies car non seulement ce n'est pas exactement le même service mais surtout ça donne parfois des interfaces bizarrement vides quand des typos non terminées sont publiées sur ce genre de sites."


### Quand publier sur cette platforme et quand publier sur une autre ?

Quand le travail  n'est qu'au stade de registre de forme et pas encore d'outil typographique (de composition textuelle). Ainsi cela guide le choix de l'auteurice sur le type de platforme où publier son travail fonderie (d'école, privée) ou recyclerie de lettres comme la nôtre. La platforme devra être un site à part entière et pas une annexe d'un fonderie d'école car elle à l'ambition de récolter des formes, décontextualisées, diverses auprès de plein d'étudianx.

> "J'ai une typo un peu crado que j'ai dessinée pour mon mémoire, les spacing sont pas hyper calibrés et il manque les chiffres, mais c'est un typo variable je pense que ça peut intéresser des gens qui cherche ce genre de formes, ça pourrait trouver sa place sur cette platforme."

<br >

---


## NOTES & RÉFLEXIONS SUR LA PLATFORME :

Plusieurs objectifs de la platforme :
    - Rendre visible des travaux en cours (ou dormant) d'étudianx
    -  Faciliter la publication de son travail en tant qu'étudianx
    - Offrir des possibilités d'utilisation, de complétion et évolution par d'autres personnes.
    

Créer une séction annexe à liste de fonts ou un site à part entière.

En tant qu’étudiant, on rend pas visible notre travail. Des fichiers pas publiés
Décomplexer les étudiants à publier leur travail
- encourager la publication de travaux
KIT: faire une liste des éléments minumum nécessaires pour publier


La typo c’est un programme. Une autre personne va l’utiliser
Rapport à la création d’outil

Exemple de typo pas finie mais qu’on a quand même utilisé

Publication de font non finie en tant qu’étudiant
Pourquoi on publie / publie parfois

---

Utilisation des fonts
Attribution du nom des auteurices des fonts + date. Pour situer la fonte
(Améliorer métadata des fonts)

Comme Git: Versionage de fonts

Paradoxe: la typo est un logiciel. Et pourtant, il n’y a pas l’approche de partage de code et copié collé ex stack overflow
FONT BOOK AMÉLIORÉ

---

Fondrie de l'école
partie "work in progress"
répertoire de formes typographiques

question: les typos se perdent, même si elles sont en libre téléchargement

qu'est-ce qu'on fait des lettres qui ne sont pas une typo ("finie", utilisable, publiée)
idée: un écran par étudianx, 

Comment présenter des projets pas finis ? --> exemple sur portfolio "Expérimentation"
