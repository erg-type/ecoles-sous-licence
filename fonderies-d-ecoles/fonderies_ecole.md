[Estienne, Paris (FR)](http://e162.eu/fonderie)
fontes pas téléchargeables mais liens vers les adresses emails des élèves

[ESAC, Cambrai (FR)](http://parametric.esac-cambrai.net/index.html)
Fontes open sources disponibles au téléchargement

[Bureau de Martine, Bétises typographiques de Cambrai (version Beta)](https://luuse.gitlab.io/workshops/www.typotheque-esac-cambrai/)
Fontes libres modifiable, disponibles en OTF

[ANRT, Nancy (FR)](https://anrt-nancy.fr/anrt-22/fr/fonts)
Fontes libres gratuites téléchargeables


[U270-D, ESAAB, Rennes (FR)](https://u270d.eesab.fr/)
Fontes libres gratuites téléchargeables


[Isdat, Toulouse (FR)](https://github.com/isdat-type/)
Scripts sur GitHub


[Esad, Valence (FR)](http://xcicero.esad-gv.net/)
Fontes en display, non disponibles au téléchargement


[erg, Bruxelles (BE)](https://chasse-ouverte.erg.be/)
Fontes en display, non disponibles au téléchargement


[La Cambre typo, Bruxelles (BE)](https://lacambretypo.be/fr/typefaces)
Fontes en display, non disponibles au téléchargement

[ESAD, Post Diplôme, Amiens (FR)](http://postdiplome.esad-amiens.fr/)
Fontes en display, non disponibles au téléchargement

[Le 75, Bruxelles (BE)](http://typotheque.le75.be/)
Fontes opensource gratuites


[HfG Karlsruhe, Karlsruhe (DE)](http://nofoundry.xyz/)
Fontes gratuites, payantes ou disponible si donation

[ECAL, Lausanne (CH)](https://ecal-typefaces.ch/)
Fontes sous licenses, payantes, une partie du revenu va à l'étudiant qui a créé la fonte


Idée : 
- Compte insta répertoriant les fonderies d'écoles (lien vers insta des fonderies, des typographes et des écoles), à l'image de @le.memo.dg, @ephemeride.fun -> 1 post pour une fonderie, (qui met en avant une font en particulier)

--> différents cas 
Faire une liste des modalités de license et paiement des fonderies d’ecole 
