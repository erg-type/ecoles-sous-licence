<h1> Index de typographies à l'usage des étudiantx en ecole d'art </h1>
Plus spécifiquement pour celleux qui ne sont ni en design graphique, ni en communication ni en typo...
Mais qui voudraient trouver une police de caractère qui soit gratuite, libre de droit : nous allons recenser ici une liste de caractères avec assez de styles différents nécéssaires à la rédaction d'un mémoire.

<li> Alegreya 
<br> Plusieurs graisses + Italic (#litterature #empattements #familleexisteaussisansempattements)
<br> Disponible sur google font
 </li>

 <li>
 Baskervville
 <br> Une seule graisse + italic (#revival #litterature #empattements)
 <br> Disponible sur google font
 </li>

 <li>
 Castoro
 <br> Une seule graisse + italic (#empattements)
 <br> Disponible sur goole font
 </li>

 <li>
 Compagnon
 <br> Une seule graisse + italic (#machine à écrire)
 <br> Disponible sur U+270D
 </li>


<li>
EB Garamond
<br> Plusieurs graisses + italiques + graisse d'italic (#litterature #gain de place #sûr de ne pas manquer de styles )
<br> Disponible sur google font
</li>

<li>
Happy Times
<br> Une graisse + italique (#litterature #empatements)
<br> Velvetyne
</li>

<li>
Jost
<br> Plusieurs graisses + italique dans differentes graisses (#sans empattements)
<br> Disponible sur google font
</li>

<li>
Libre Caslon
<br> Plusieurs graisses + italique (#empattements)
<br> Disponible sur google font
</li>

<li>
Newsreader
<br> Plusieurs graisses + italique + italiques dans différentes graisses (#empattements)
<br> Disponible sur google font
</li>

<li>
Nimbus Roman n°9
<br> Plusieurs graisses + italique + italique dans différentes graisses (#empattements)
<br> Disponible sur 1001 font
</li>

<li>
Spectral
<br> Plusieurs graisses + italique + italique dans différentes graisses (#empattements)
<br> Disponible sur goole font
</li>

    Je télécharge une font libre  pour mon mémoire, mais elle est monostyle comme elle est gratuite

    Mais j'ai besoin d'italique pour les titres d’œuvres et d'une graisse différente pour les chapitres de mon mémoire, et je suis certainement trop pauvre pour acheter toutes les familles

    J'ai le droit d'incliner la fonte dans indesign pour faire semblant, mais ce n'est pas très sexy.

    Encore pire pour la graisse, j'ai rajouté un contour dans indesign mais les lettres de la typo deviennent un peu pâteuses.

Résumé : Je télécharge la ZT Gata sur le site fontesk (license normalement libre sur ce site) mais je ne peux pas payer et il n'y a que la demi bold mais il me faut vrmt cette typo qui correpond à la forme de mon mémoire et à son propos (mon mémoire est un magazine) et je n'ai pas envie d'avoir la même typo que 36 autres personnes qui font des mémoires sur les magazines

// je lis la license car je suis une bonne personne //

ZT Gatha is a continuation of the "Gatha Duo font family", which focuses only on more complete variations of sans serif, this font has two variants, one of which is a normal sans font and the other is unique in that it minimizes edges. of each letter produces a unique character with a smoothly curved groove.

// la description me vends du rêve et je VEUX vrmt cette typo //

ZT Gatha features 10 styles covering a complete set of beautiful upper and lower case letters, numbers, and various punctuation marks, providing a clean and realistic sans serif style with great versatility. With a lot of weight, this typeface can be used successfully in Magazines, Posters, Branding, Websites, etc.

// je découvre qu'il y a 10 styles donc c'est super, peut-être que je vais trouver ce qu'il me faut //

I hope you have fun using Gatha.

ZT Gatha Semi Bold is free for personal and commercial

// c'est syper c'est free mais il n'y a qu'un style dispo //

Buy ZT Gatha font family:
https://creativemarket.com/zelowtype/12717410-ZT-Gatha

// super ! elle est pas chère, je peux me permettre finalement d'acheter d'autres graisses... mais je n'en ai besoin que d'une seule et ça me gêne de tout payer alors qu'il n'y a pas l'italique que je recherche désespérément //

Support the designer:
https://zelowtype.gumroad.com/l/ztgatha

// peut être que je peux envoyer un peu d'argent au designer pour me sentir bien et cracker juste la graisse dont j'ai besoin...//

Like and follow:
https://www.instagram.com/zelowtype
https://www.behance.net/zelowtype

// ça à l'air d'être qlq de bien je vais donc acheter ses 10 graisses différentes//

// je termine de lire la license car je suis qlq de bien //

    Je suis le lien vers la version payante, je trouve les graisses. Peut-être que je peux acheter un seul style ? Le contact du designer est dans la licence et la typo n'existe pas en italique ? 

    Je lui envoie un mail pour savoir s'il va la dessiner un jour ?

    Je lui envoie un mail pour savoir si je peux la redessiner en italique en lui expliquant que mon mémoire est un objet qui va rester enfuit dans la bibliothèque de mon école. Dans ce cas c'est vrai que c'est plus sympa si j'ai déjà payé me permettre de lui envoyer un message pour l'italique

    J'abandonne et je change de typo ?

    Finalement l’effet penché de indesign c'est pas si mal ?

    Je change de typo pour les italiques de mon mémoire ? Avec une typo à empattements pour contraster ? 

    Je décide de souligner dans indesign mes titres.

    je change de couleur pour mes titres

    je n'écris pas mon mémoire et décide de l'enregistrer sur cassette audio

    finalement, 10 graisses différentes c'est quand même pas mal, je peux peut-être m'en sortir pour mes titres, italiques pour les titres c'est so à l'ancienne

    dans mon colophon je dis que mon italique en fait c'est une autre graisse ?

    pour ma prochaine recherche de typo je coche la case "disponible en italique"

    je cherche les ressources dispo, est-ce qu'il existe une liste de typo labeur libre de droit avec italique suffisamment complète pour composer texte, plus large que juste en latin pour l'usage du mémoire ? (usage spécifique du mémoire universitaire, projet écriture, essai), 

    il faudrait crée un moteur de recherche de typographie de labeur pour les mémoires/essais/textes, 

    dans lequels ont est sûre d'avoir une typo libre de droit avec deux graisses différentes et un italique

<li>
Work sans
<br> Plusieurs graisses + italique + italiques dans differentes graisses (#sansserif)
